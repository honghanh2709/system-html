
// Js Menu mobile header 1
$(".menu-mobile .ti-menu").click(function () {
    $('.header-1').addClass('show-menu');
});
$(".close-menu").click(function () {
    $('.header-1').removeClass('show-menu');
});

//Search mobile
$(".btn-search .ti-search").click(function () {
    $('.btn-search').addClass('show-search-mb');
});
$(".close-search").click(function () {
    $('.btn-search').removeClass('show-search-mb');

});
// End Js Menu mobile header 1



// Js Menu mobile header 2
$(".menu-mobile-2 .ti-menu").click(function () {
    $('.header-2').addClass('show-menu');
});
$(".close-menu-2").click(function () {
    $('.header-2').removeClass('show-menu');
});

//Search mobile
$(".btn-search-2 .ti-search").click(function () {
    $('.btn-search-2').addClass('show-search-mb');
});
$(".btn-search-2 .close-search").click(function () {
    $('.btn-search-2').removeClass('show-search-mb');

});
// End Js Menu mobile header 2


// Js Menu mobile header 3
$(".menu-mobile-3 .ti-menu").click(function () {
    $('.header-3').addClass('show-menu');
});
$(".close-menu-3").click(function () {
    $('.header-3').removeClass('show-menu');
});

//Search mobile header 3
$(".btn-search-mb .ti-search-mb").click(function () {
    $('.header-3').addClass('show-search-mb');
});
$(".close-search-3").click(function () {
    $('.header-3').removeClass('show-search-mb');

});
// End Js Menu mobile header 3


// Js Menu mobile header 4
$(".menu-mobile .ti-menu").click(function () {
    $('.header-4').addClass('show-menu');
});
$(".close-menu").click(function () {
    $('.header-4').removeClass('show-menu');
});

//Search mobile
$(".btn-search .ti-search").click(function () {
    $('.btn-search').addClass('show-search-mb');
});
$(".close-search").click(function () {
    $('.btn-search').removeClass('show-search-mb');

});
// End Js Menu mobile header 4


// Js Menu mobile header 5
$(".menu-mobile-5 .ti-menu").click(function () {
    $('.header-5').addClass('show-menu');
});
$(".close-menu-5").click(function () {
    $('.header-5').removeClass('show-menu');
});

// End Js Menu mobile header 5


// Js Menu mobile header 6
$(".menu-mobile-6 .ti-menu").click(function () {
    $('.header-6').addClass('show-menu');
});
$(".close-menu-6").click(function () {
    $('.header-6').removeClass('show-menu');
});

// End Js Menu mobile header 6


// Js Menu mobile header 7
$(".menu-mobile-7 .ti-menu").click(function () {
    $('.header-7').addClass('show-menu');
});
$(".close-menu-7").click(function () {
    $('.header-7').removeClass('show-menu');
});

// End Js Menu mobile header 7


// Js Menu mobile header 8
$(".menu-mobile-8 .ti-menu").click(function () {
    $('.header-8').addClass('show-menu');
});
$(".close-menu-8").click(function () {
    $('.header-8').removeClass('show-menu');
});

// End Js Menu mobile header 8


// Js Menu mobile header 9
$(".menu-mobile-9 .ti-menu").click(function () {
    $('.header-9').addClass('show-menu');
});
$(".close-menu-9").click(function () {
    $('.header-9').removeClass('show-menu');
});

// End Js Menu mobile header 9


// Js Menu mobile header 10
$(".menu-mobile-10 .ti-menu").click(function () {
    $('.header-10').addClass('show-menu');
});
$(".close-menu-10").click(function () {
    $('.header-10').removeClass('show-menu');
});

//Search mobile header 10
$(".btn-search-mb .ti-search-mb").click(function () {
    $('.header-10').addClass('show-search-mb');
});
$(".close-search-10").click(function () {
    $('.header-10').removeClass('show-search-mb');

});

// End Js Menu mobile header 10


// Sticky menu tab  to srcoll page lisst service 3

$(document).ready(function () {
    $(".menu-tab-service-3 ul li a").click(function () {
        $(".menu-tab-service-3 ul li a").removeClass("show-active");
        $(this).addClass("show-active");
    });
});